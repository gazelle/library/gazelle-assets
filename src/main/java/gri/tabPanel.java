package gri;

import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.component.UIComponentBase;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;

public class tabPanel extends UIComponentBase {

	public static final String CLASS = "class";

	private enum PropertyKeys {
		CONTENT
	};

	public String getContent() {
		return (String) getStateHelper().get(PropertyKeys.CONTENT);
	}

	public void setContent(String newValue) {
		getStateHelper().put(PropertyKeys.CONTENT, newValue);
	}
	
	public void encodeBegin(FacesContext context) throws java.io.IOException {
		String activeItem = TabPanelHelper.getActiveItem(this);
		ResponseWriter writer = context.getResponseWriter();
		List<UIComponent> lcomp = this.getChildren();
		writer.startElement("ul", this);
		writer.writeAttribute("role", "tablist", null);
		writer.writeAttribute(CLASS, "nav nav-tabs", null);
		for (UIComponent uiComponent : lcomp) {
			if (uiComponent instanceof tab && uiComponent.isRendered()) {
				writer.startElement("li", this);
				writer.writeAttribute("role", "presentation", null);
				if (activeItem != null && activeItem.equals(TabHelper.getIdTab((tab)uiComponent))) {
					writer.writeAttribute(CLASS, "active", null);
					((tab)uiComponent).setInitialActiveItem(true);
				}
				if (TabHelper.getOnClick((tab)uiComponent) != null) {
					writer.writeAttribute("onclick", TabHelper.getOnClick((tab)uiComponent), null);
				}
				writer.startElement("a", this);
				writer.writeAttribute("href", "#" + TabHelper.getIdTab((tab)uiComponent), null);
				writer.writeAttribute("aria-controls", TabHelper.getIdTab((tab)uiComponent), null);
				writer.writeAttribute("role", "tab", null);
				writer.writeAttribute("data-toggle", "tab", null);
				writer.writeText(TabHelper.getHead((tab)uiComponent), null);
				writer.endElement("a");
				writer.endElement("li");
			}
		}
		writer.endElement("ul");
		writer.startElement("div", this);
		if (TabPanelHelper.isWithBorder(this)) {
			writer.writeAttribute(CLASS, "tab-content tab-content-with-border", null);
		}
		else {
			writer.writeAttribute(CLASS, "tab-content", null);
		}
	}

	public void encodeEnd(FacesContext context) throws java.io.IOException {
		ResponseWriter writer = context.getResponseWriter();
		writer.endElement("div");
		writer.write("\n"); 
	}

	@Override
	public String getFamily() {
		return "gri.tabPanel";
	}

}