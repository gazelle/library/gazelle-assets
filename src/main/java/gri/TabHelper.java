package gri;

public class TabHelper {
	
	public static String getIdTab(tab uitab) {
		return (String) uitab.getAttributes().get("id");
	}

	public static String getHead(tab uitab) {
		return (String) uitab.getAttributes().get("head");
	}
	
	public static String getOnClick(tab uitab) {
		return (String) uitab.getAttributes().get("onclick");
	}
	
}
