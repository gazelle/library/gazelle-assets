package gri;

import java.util.List;

import javax.faces.component.UIComponent;

public class TabPanelHelper {
	
	public static boolean isWithBorder(tabPanel uitabPanel) {
		try {
			Boolean withBorder = (Boolean) uitabPanel.getAttributes().get("withBorder");
			if (withBorder != null) {
				return withBorder;
			}
		}
		catch(Exception e) {
			return false;
		}
		return false;
	}
	
	public static String getActiveItem(tabPanel uitabPanel) {
		String activeItem = null;
		try {
			activeItem = (String) uitabPanel.getAttributes().get("activeItem");
			if (activeItem != null) {
				return activeItem;
			}
		}catch(Exception e) {}
		List<UIComponent> lchild = uitabPanel.getChildren();
		for (UIComponent uiComponent : lchild) {
			if (uiComponent instanceof tab) {
				activeItem = TabHelper.getIdTab((tab)uiComponent);
				break;
			}
		}
		return activeItem;
	}
	
}
