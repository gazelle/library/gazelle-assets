var applicationUrlBaseName;
var pingTime;
var nextPingTimeout;

var vis = (function () {
    var stateKey, eventKey, keys = {
        hidden: "visibilitychange",
        webkitHidden: "webkitvisibilitychange",
        mozHidden: "mozvisibilitychange",
        msHidden: "msvisibilitychange"
    };
    for (stateKey in keys) {
        if (stateKey in document) {
            eventKey = keys[stateKey];
            break;
        }
    }
    return function (c) {
        if (c) document.addEventListener(eventKey, c);
        return !document[stateKey];
    }
})();


function schedulePing(applicationUrlBaseNameParam, pingTimeParam) {
    applicationUrlBaseName = applicationUrlBaseNameParam;
    pingTime = 1000 * pingTimeParam;
    scheduledPing();
}

function scheduledPing() {
    ping();
    nextPingTimeout = setTimeout("scheduledPing();", pingTime);
}

function ping() {
    if (vis()) {
        performPing();
    }
}


function performPing() {
    var nowDate = new Date();
    var nowTime = nowDate.getTime();
    var offset = nowDate.getTimezoneOffset();
    var data = {
        'now': nowTime,
        'offset': offset
    };
    jQuery.ajax({
        url: '/' + applicationUrlBaseName + '/ping.seam',
        data: data,
        success: function (result, status) {
            if (status == 'success') {
                window.eval(result);
            }
        }
    });
}

