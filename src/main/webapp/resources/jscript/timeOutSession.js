window.onload = function () {
    document.addEventListener('DOMContentLoaded', function () {
        console.log('Load eventListener');
        // Create a function to handle AJAX errors
        function handleAjaxError(xhr, status, error) {
            if (xhr.status === 405) {
                console.log('status == 405');
                document.getElementById('timeOutSessionModalPanel').style.display = 'block';
            } else if (status === 'error' && xhr.status === 0 && xhr.statusText === 'error') {
                console.log('status === \'error\' && xhr.status === 0 && xhr.statusText === \'error\'');
                document.getElementById('timeOutSessionModalPanel').style.display = 'block';
            } else {
                console.log('else');
                document.getElementById('timeOutSessionModalPanel').style.display = 'block';
            }
        }

        // Override the default XMLHttpRequest send method
        (function (open) {
            XMLHttpRequest.prototype.open = function (method, url, async, user, pass) {
                this.addEventListener('readystatechange', function () {
                    if (this.readyState === 4 && this.status !== 200) {
                        handleAjaxError(this, 'error', this.statusText);
                    }
                }, false);
                open.call(this, method, url, async, user, pass);
            };
        })(XMLHttpRequest.prototype.open);
    });
}